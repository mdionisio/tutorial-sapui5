sap.ui.define([
	"ClientApp/Demo/controller/BaseController",
	"sap/ui/model/odata/ODataModel"
], function (Controller, ODataModel) {
	"use strict";
	
	var oUselessDialog;
	
	return Controller.extend("ClientApp.Demo.controller.ProductList", {

		onInit: function() {
			var oDataModel = new ODataModel("/Northwind", true);
			this.getView().setModel(oDataModel);
			
			oUselessDialog = sap.ui.xmlfragment("ClientApp.Demo.fragment.UselessDialog", this);
			this.getView().addDependent(oUselessDialog);
		},
		
		onPressOk: function(oEvent) {
			var oRejectBtn = this.getView().byId("reject");
			oRejectBtn.setVisible(!oRejectBtn.getVisible());
			
			oEvent.getSource().setText(oRejectBtn.getVisible() ? this.getText("disableRedButton") : this.getText("enableRedButton"));                         
		},
		
		onPressReject: function() {
			oUselessDialog.open();
		},
		
		onPressUselessDialog: function(oEvent) {
			oUselessDialog.close();
        }

	});

});