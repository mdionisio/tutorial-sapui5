sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {

	return Controller.extend("ClientApp.Demo.controller.BaseController", {

        getText: function(sText) {
        	return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sText);
        }
        
	});
});