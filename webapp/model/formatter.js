sap.ui.define(function() {
	"use strict";

	var formatter = {
		productStatus: function (bDiscontinued) {
			return bDiscontinued ? sap.ui.core.ValueState.Error : sap.ui.core.ValueState.Success;
		},
		
		productStockIcon: function(iStock) {
			return iStock > 0 ? "sap-icon://sys-enter" : "sap-icon://message-error";
		}
	};
	
	return formatter;

}, true);
